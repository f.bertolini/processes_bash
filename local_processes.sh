#!/bin/bash
#########################################################
#	                            						#
#	Check if processes / docker containers run	        #
#		by f.bertolini AT riseup.net 		            #
#########################################################

# Configuration Parameters

declare -a PROCESSES=("[m]ongod" "[a]pache" "[d]ocker" "[n]ode")
declare -a CONTAINERS=()

FILE=~/logs/processes.txt

ADMIN_MAIL="admin@mail.com"
GMAIL_USER="account@gmail.com"
GMAIL_PASSWORD="mypassword"

##########################################################
# Colors
DANGER="\033[0;31m"
SUCCESS="\033[0;32m"
WARNING="\033[1;33m"
NC="\033[0m"

##########################################################

OUTPUT=$(echo -e "--== Processes Running LOGS ==--")"\n"
ERROR=false

function printer {
    echo -e $1
    OUTPUT+=$(echo -e $1)"\n"
}


#check processes
printer ".:::: Check Processes ::::."
if (("${#PROCESSES[@]}">0))
then
for i in "${PROCESSES[@]}"; do
	if pgrep "$i" >/dev/null 2>&1
		then printer "$i\t\t[${SUCCESS}OK${NC}]"
		else 
			printer "$i\t\t[${DANGER}FAIL${NC}]"    
			ERROR=true
	fi
done

else printer "${WARNING}No Processes in List${NC}"
fi
#check containers
printer ".:::: Check Docker Containers ::::."
if pgrep "[d]ocker" >/dev/null 2>&1
	then 
		if (("${#CONTAINERS[@]}">0)) 
			then 
				C=0
				for CONTAINER in "${CONTAINERS[@]}"; do
					RUNNING=$(docker inspect --format="{{ .State.Running }}" $CONTAINER 2> /dev/null)
				    if [ $? -eq 1 ]; 
                        then
					        #not exist
                            printer "$CONTAINER\t\t[${DANGER}NOT EXIST${NC}]" ; ERROR=true
					    else 
					        if ($RUNNING)
						        then printer "$CONTAINER\t\t[${SUCCESS}RUN${NC}]"
						        else printer "$CONTAINER\t\t[${DANGER}STOP${NC}]"; ERROR=true
		                    fi
					fi
				done
			else printer "${WARNING}No Containers in List${NC}"
		fi
    else
	    printer "${WARNING}Docker is not running...${NC}"
	    ERROR=true
fi

echo -e $OUTPUT | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" > ${FILE} 

if $ERROR
    then 
        printer ".:::: Send Mail to Admin ::::."
	    GMAIL_LOGIN=$GMAIL_USER:$GMAIL_PASSWORD
   	    curl --silent --url "smtps://smtp.gmail.com:465" --ssl-reqd --mail-from "${GMAIL_USER}" --mail-rcpt "${ADMIN_MAIL}" --upload-file $FILE --user ${GMAIL_LOGIN} --insecure
	    #rm ${FILE}
	    printer ".:::: Mail Sent ::::."
    else 
        exit 0
fi
