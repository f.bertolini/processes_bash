#!/bin/bash
#########################################################
#                           							#
#	Check remote if Hosts/Sites are Reachable	        #
#		by f.bertolini AT riseup.net 		            #
#########################################################

# Configuration Parameters

declare -a IPS=("127.0.0.1" "8.8.8.8")
declare -a WEBSITES=("google.it" "tiscali.it" "fake.nj")

FILE=~/logs/remote.txt

ADMIN_MAIL="admin@mail.com"
GMAIL_USER="account@gmail.com"
GMAIL_PASSWORD="mypassword"

##########################################################
# Colors
DANGER="\033[0;31m"
SUCCESS="\033[0;32m"
WARNING="\033[1;33m"
NC="\033[0m"

##########################################################

OUTPUT=$(echo -e "--== Remote Hosts LOGS ==--")"\n"
ERROR=false

function printer {
    echo -e $1
    OUTPUT+=$(echo -e $1)"\n"
}

printer ".:::: Ping IPs ::::."
if (("${#IPS[@]}">0))
    then
        for i in "${IPS[@]}"; do
	        if ping -c 1 $i &> /dev/null
	        then
	            printer "${i}\t\t[${SUCCESS}OK${NC}]"
	        else
	            printer "${i}\t\t[${DANGER}FAIL${NC}]"
	            ERROR=true
            fi
        done
    else
        printer "${WARNING}No IPs in List${NC}"
fi

printer ".:::: Check Websites ::::."
if (("${#WEBSITES[@]}">0))
    then
        for i in "${WEBSITES[@]}"; do
	        if curl $i -s -f -o /dev/null
	        then
	            printer "${i}\t\t[${SUCCESS}UP${NC}]"
	        else
	            printer "${i}\t\t[${DANGER}DOWN${NC}]"
	            ERROR=true
            fi
        done
    else printer "${WARNING}No Sites in List${NC}"
fi

echo -e $OUTPUT | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" > ${FILE}

if $ERROR
    then 
	    printer ".:::: Send Mail to Admin ::::."
	    GMAIL_LOGIN=$GMAIL_USER:$GMAIL_PASSWORD
   	    curl --silent --url "smtps://smtp.gmail.com:465" --ssl-reqd --mail-from "${GMAIL_USER}" --mail-rcpt "${ADMIN_MAIL}" --upload-file ${FILE} --user ${GMAIL_LOGIN} --insecure
	    printer ".:::: Mail Sent ::::."
    else 
        exit 0
fi
